def add(x, y):
    return x + y


def substract(x, y):
    return x - y


print("Select an operation:")
print("1. Add")
print("2. Substract")
print("3. Multiply")
print("4. Divide")

choice = input("Enter an operation (1,2,3,4): ")

number1 = int(input("Enter first number:"))
number2 = int(input("Enter second number:"))

if choice == '1':
    print(number1, ' + ', number2, ' = ', add(number1, number2))
elif choice == '2':
    print(number1, ' - ', number2, ' = ', substract(number1, number2))
elif choice == '3':
    print(" << Not implemented yet! >>".upper())
elif choice == '4':
    print(" << Not implemented yet! >>".upper())
else:
    print(" << Invalid input >>".upper())
